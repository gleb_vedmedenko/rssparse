import java.io.FileWriter;
import java.io.File;
import java.io.FileReader;
import java.util.*;
import com.opencsv.*;

import javafx.scene.control.Alert;

import java.io.IOException;
/*Classe qui crée un fichier de configuration.
  Ce fichier stocke des informations sur le flux RSS*/
public class Config extends MyFile {

	
	public Config(String path,String name)
	{
		super(name,path);
	}
	//l'ajout d'un nouveau flux à la base de données
	public boolean AddNewFlux(String url,String path,String title)
	{
		String[] line = {path,url,title};
		File file = new File(super.getFullPath());
		if(file.length() > 0)
		{
			if(!checkURL(url))
			{
				write(line);
				return true;
			}
				
			else
			{
				showAlertWarning();
				return false;
			}
				
		}
		else
		{
			write(line);
			return true;
		}
		
	}
	//Obtenir des références de flux RSS à partir de la base de données
	public List<String> getLinks()
	{
		List<String> links = new ArrayList<String>();
		 try(CSVReader reader = new CSVReader(new FileReader(super.getFullPath())))
    	 {
    		 List<String[]> rows = reader.readAll();
             for (String[] row: rows) {
            	 links.add(row[1]);
            	 System.out.println(row[1]);	
            		  
             }
    	 }
    	 catch (IOException e) {

 			e.printStackTrace();
 		}
		 return links;
	}
	//La méthode permettant de détecter la présence d'un lien dans la base de données
	public boolean checkURL(String link)
	{
		
		 try(CSVReader reader = new CSVReader(new FileReader(super.getFullPath())))
		 
    	 {
			 
    		 List<String[]> rows = reader.readAll();
             for (String[] row: rows) {
            	 for(String s: row)
            	 {
            		 if(link.equals(s))
            			 return true;
            	 }
             }
             
    	 }
    	 catch (IOException e) {
 			
 			e.printStackTrace();
 		}
		 return false;
	}
	// les méthodes de lecture et d'écriture de fichiers
	@Override
	public void write(String[] entries)
	{
		 //la création d'objets CSVWriter par le cadre Opencsv
		try (CSVWriter writer = new CSVWriter(new FileWriter(super.getFullPath(),true))) {
   		 	
            writer.writeNext(entries);
            
        } catch (IOException e) {
			
			e.printStackTrace();
		}
   	
	}
	@Override
	public String[] read()
	{
		 try(CSVReader reader = new CSVReader(new FileReader(super.getFullPath())))
    	 {
    		 List<String[]> rows = reader.readAll();
             String[] str = null;
             for (String[] row: rows) {
            	 str = row;
             }
             return str;
    	 }
    	 catch (IOException e) {
 			e.printStackTrace();
 		}
		 return null;
	}
	// les méthodes de notification d'utilisateur
	private void showAlertWarning() { 
	    Alert alert = new Alert(Alert.AlertType.WARNING);
	    alert.setTitle("Warning");
	    alert.setHeaderText(null);
	    alert.setContentText("Rss-chanel already exist in data base");
	    alert.showAndWait();
	}
	public void showAlertInfo() { 
	    Alert alert = new Alert(Alert.AlertType.INFORMATION);
	    alert.setTitle("Message");
	    alert.setHeaderText(null);
	    alert.setContentText("New rss-chanel successfully added ");
	    alert.showAndWait();
	}

}
