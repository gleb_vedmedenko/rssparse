import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;
import java.net.MalformedURLException;
import java.net.URL;

import org.junit.Test;

public class UnitTest {
FeedMessage feed = new FeedMessage();
MyFile file = new CSVFile(System.getProperty("user.dir"),"\\Test.txt");
RSSFeedParser parseFeed = new RSSFeedParser("http://feeds.skynews.com/feeds/rss/world.xml");
@Test
public void isFieldsEmpty()
{
	
	assertEquals(feed.author,null);
	assertEquals(feed.date,null);
	assertEquals(feed.description,null);
	assertEquals(feed.link,null);
	assertEquals(feed.rss,null);
	assertEquals(feed.source,null);
	assertEquals(feed.title,null);
}
@Test
public void isNotNullFile()
{
	assertNotEquals(file.file,null);
}
@Test
public void isRightPath()
{
	assertEquals(file.getName(),"\\Test.txt");
	assertEquals(file.getPath(),System.getProperty("user.dir"));
}
@Test
public void isRightURLType() throws MalformedURLException
{
	URL url = new URL("http://feeds.skynews.com/feeds/rss/world.xml");
	assertTrue(parseFeed.url.equals(url));
}
@Test
public void isRightParserFields() 
{
	assertEquals(RSSFeedParser.TITLE, "title");
	assertEquals(RSSFeedParser.SOURCE, "source");
	assertEquals(RSSFeedParser.RSS, "rss");
	assertEquals(RSSFeedParser.LINK, "link");
	assertEquals(RSSFeedParser.DESCRIPTION, "description");
	assertEquals(RSSFeedParser.DATE, "pubDate");
	assertEquals(RSSFeedParser.AUTHOR, "author");
}
@Test
public void NotNullUrlAtStart()
{
	RSSFeedParser feed = new RSSFeedParser("http://feeds.skynews.com/feeds/rss/world.xml");
	assertNotNull(feed.url);
}
}
