import java.io.FileWriter; 
import java.io.FileReader;
import java.util.*;
import com.opencsv.*;
import java.io.IOException;
// la classe pour créer un fichier csv
public class CSVFile extends MyFile {

	  
     String filePath = "";
    
     public CSVFile(String path,String fileName)
     {
    	 super(fileName,path);
    	 this.filePath =  path + "\\" + fileName +".csv";
    	
     }
     // les méthodes de lecture et d'écriture de fichiers
     @Override
     public void write(String[] entries)
     {
    	 //la création d'objets CSVWriter par le cadre Opencsv
 try (CSVWriter writer = new CSVWriter(new FileWriter(filePath,true))) {
    		 
             writer.writeNext(entries);
             
         } catch (IOException e) {
			
			e.printStackTrace();
		}
     }
     @Override
     public String[] read()
     {
    	 try(CSVReader reader = new CSVReader(new FileReader(filePath)))
    	 {
    		 List<String[]> rows = reader.readAll();
             
             
    	 }
    	 
    	 catch (IOException e) {
 			
 			e.printStackTrace();
 		}
		return null;
     }
     
	
}
             
 
	
	

