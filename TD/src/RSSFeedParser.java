import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.XMLEvent;

import org.jsoup.Jsoup;

// La classe qui effectue l'analyse syntaxique
public class RSSFeedParser {
	
        static final String TITLE = "title";
        static final String DESCRIPTION = "description";
        static final String RSS = "rss";
        static final String SOURCE = "source";
        static final String LINK = "link";
        static final String AUTHOR = "author";
        static final String ITEM = "item";
        static final String DATE = "pubDate";
        
        
        final URL url;

        public RSSFeedParser(String feedUrl) {
                try {
                        this.url = new URL(feedUrl);
                } catch (MalformedURLException e) {
                        throw new RuntimeException(e);
                }
        }
        // La méthode qui lit de fichier RSS
        public Feed readFeed() {
                Feed feed = null;
                try {
                        boolean isFeedHeader = true;
                        // Définir les valeurs d'en-tête initiales à la chaîne vide
                        String description = "";
                        String title = "";
                        String link = "";
                        String rss = "";
                        String source= "";
                        String author = "";
                        String date = "";
                        

                        // Création un nouveau XMLInputFactory
                        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                        //Configurer un nouvel événementReader
                        
                        inputFactory.setProperty(XMLInputFactory.IS_COALESCING, Boolean.TRUE);
                        InputStream in = read();
                        
                        XMLEventReader eventReader = inputFactory.createXMLEventReader(in,"UTF-8");
                        
                        // Lire le document XML
                        while (eventReader.hasNext()) {
                                XMLEvent event = eventReader.nextEvent();
                                
                                if (event.isStartElement()) {
                                        String localPart = event.asStartElement().getName()
                                                        .getLocalPart();
                                        switch (localPart) {
                                        
                                        case ITEM:
                                                if (isFeedHeader) {
                                                        isFeedHeader = false;
                                                        feed = new Feed(title, link, description, author,
                                                                        rss,date,source);
                                                }
                                                event = eventReader.nextEvent();
                                                break;
                                        case TITLE:
                                        		if(title == "")
                                                title = getCharacterData(event, eventReader);
                                        		if(rss == "")
                                        		rss = getCharacterData(event, eventReader);
                                                break;
                                        case DESCRIPTION:
                                        		if(description == "")
                                                description = getCharacterData(event, eventReader);
                                                break;
                                        
                                        case LINK:
                                        		if(link == "")
                                                link = getCharacterData(event, eventReader);
                                                break;
                                        
                                        case AUTHOR:
                                        		if(author == "")
                                                author = getCharacterData(event, eventReader);
                                                break;
                                        case DATE:
                                        		if(date == "")
                                                date = getCharacterData(event, eventReader);
                                                break;
                                        
                                        }
                                } else if (event.isEndElement()) {
                                        if (event.asEndElement().getName().getLocalPart() == (ITEM)) {
                                        	// l'initialisation de l'objet qui stocke des informations
                                        		FeedMessage message = new FeedMessage();
                                                message.setAuthor(author,"\t");
                                                message.setDescription(description,"\t");
                                                message.setRss(rss,"\t");
                                                message.setLink(link,"\t");
                                                message.setTitle(title,"\t");
                                                message.setSource(url.toString(),"\t");
                                                message.setDate(date,"\t");
                                                description = "";
                                                title = "";
                                                link = "";
                                                source = "";
                                                author = "";
                                                date = "";
                                                //sauvegarder la base de données
                                                feed.getMessages().add(message);
                                                event = eventReader.nextEvent();
                                                continue;
                                        }
                                }
                        }
                } catch (XMLStreamException e) {
                        throw new RuntimeException(e);
                }
                return feed;
        }
        // la méthode qui permet d'obtenir le nom de fichier Rss
        public String getFeedName()
        {
            Feed feed = null;
            String title = "";
            try {
                    boolean isFeedHeader = true;
                    XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                    inputFactory.setProperty(XMLInputFactory.IS_COALESCING, Boolean.TRUE);
                    InputStream in = read();
                    XMLEventReader eventReader = inputFactory.createXMLEventReader(in,"UTF-8");
                    while (eventReader.hasNext()) {
                            XMLEvent event = eventReader.nextEvent();
                            if (event.isStartElement()) {
                                    String localPart = event.asStartElement().getName()
                                                    .getLocalPart();
                                    switch (localPart) {
                                    
                                    case ITEM:
                                            if (isFeedHeader) {
                                                    isFeedHeader = false;
                                            }
                                            event = eventReader.nextEvent();
                                            break;
                                    case TITLE:
                                            title = getCharacterData(event, eventReader);
                                            return title;
                                    
                                    
                                    }
                            } else if (event.isEndElement()) {
                                    if (event.asEndElement().getName().getLocalPart() == (ITEM)) {
                                            if(title == "")
                                            	System.out.println("NULL");
                                            else
                                            	return title;
                                    }
                            }
                    }
            } catch (XMLStreamException e) {
                    throw new RuntimeException(e);
            }
            return title;
        }
       
        // la méthode qui extrait de données
        private String getCharacterData(XMLEvent event, XMLEventReader eventReader)
                        throws XMLStreamException {
                String result = "";
                event = eventReader.nextEvent();
                
                if (event instanceof Characters) {
                        result = event.asCharacters().getData();
                        // la suppression des 	balises HTML par le  Jsoup
                        result = Jsoup.parse(result).text();
                }
                return   result;
        }

        private InputStream read() {
                try {
                        return url.openStream();
                } catch (IOException e) {
                        throw new RuntimeException(e);
                }
        }
}