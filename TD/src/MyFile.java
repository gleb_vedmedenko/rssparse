import java.io.File;
// La Classe pour créer différents types de fichiers
public abstract class MyFile {

	private String name;
	private String path;
	private String url;
	private String fullPath;
	public File file;
	public MyFile(String name,String path)
	{
		setPath(path);
		setName(name);
		setFullPath(path,name);
		file = new File(getPath(),getName());
		
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	public void setPath(String path)
	{
		this.path = path;
	}
	public void setURL(String url)
	{
		this.url = url;
	}
	public String getURL()
	{
		return this.url;
	}
	public String getName()
	{
		return this.name;
	}
	public String  getPath()
	{
		return this.path;
	}
	public void setFullPath(String path,String name)
	{
		this.fullPath = path  + name;
	}
	public String getFullPath()
	{
		return this.fullPath;
	}
	//Les méthodes de substitution
	public abstract void write(String []entries);
	public abstract String[] read();
	
}
