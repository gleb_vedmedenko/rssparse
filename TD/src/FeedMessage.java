//Représente un message RSS
public class FeedMessage {

        String title;
        String description;
        String link;
        String author;
        String rss;
        String source;
        String date;
        public String getTitle() {
        
                return title;
        }

        public void setTitle(String title,String separator) {
        		
                this.title = title + separator;
        }

        public String getDescription() {
                return description;
        }

        public void setDescription(String description,String separator) {
                this.description = description + separator;
        }

        public String getLink() {
                return link;
        }

        public void setLink(String link,String separator) {
                this.link = link+ separator;
        }

        public String getAuthor() {
        	
                return author;
        }

        public void setAuthor(String author,String separator) {
                this.author = author+separator;
        }

        public String getRss() {
        	
                return rss;
        }

        public void setRss(String rss,String separator) {
                this.rss = rss+ separator;
        }
        public String getSource() {
            return source;
    }
        public String getDate()
        {
        	return date;
        }
        public void setDate(String date,String separator)
        {
        	this.date = date + separator;
        }

        public void setSource(String source,String separator) {
            this.source= source + separator;
    }
        public void setParsed(String title,String description,String date,
        		String author,String link,String source,String rss)
        {
        this.title = title;
        this.description= description;
        this.date = date;
        this.author = author;
        this.link = link;
        this.source = source;
        this.rss = rss;
        	
        }
        @Override
        public String toString() {
                return "FeedMessage [title=" + title + " description= " + description
                                + " link= " + link + ", author=" + author + " rss= " + rss
                                + "]";
        }

}