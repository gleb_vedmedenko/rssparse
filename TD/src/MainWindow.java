import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/*La classe initie la création de l’interface 
et unit la logique de programme*/
public class MainWindow extends Application{
	static boolean isEmptyBase = true;
	//le chemin d'accès au dossier d'utilisateur
	static String config_path = System.getProperty("user.dir");
	static String folder_path = config_path;
	static String url_path = "";
	// La collection des liens RSS pour le fichier de configuration 
	static List<String> links = new ArrayList<String>();
	// La collection des liens RSS pour le fichier de métadonnées
	static List<String> metaLinks = new ArrayList<String>();
	Config config;
	String[] str = null;
	// les colonnes d'interface
	TableView<FeedMessage> table = new TableView<FeedMessage>();
	TableColumn<FeedMessage, String> Title = new TableColumn<FeedMessage, String>("Tilte");
	TableColumn<FeedMessage, String> Description = new TableColumn<FeedMessage, String>("Description");
	TableColumn<FeedMessage, String> Link = new TableColumn<FeedMessage, String>("Link");
	TableColumn<FeedMessage, String> Date= new TableColumn<FeedMessage, String>("Date");
	TableColumn<FeedMessage, String> Rss = new TableColumn<FeedMessage, String>("Rss");
	TableColumn<FeedMessage, String> Author = new TableColumn<FeedMessage, String>("Author");
	TableColumn<FeedMessage, String> Source = new TableColumn<FeedMessage, String>("Source");
	// La collection pour l'affichage
	ObservableList<FeedMessage> list;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}
	Stage window;
	Scene scene;
	
	@Override
	public void start(Stage primaryStage) throws Exception {

		
		window = primaryStage;
		window.setTitle("CSV Generator");
		
		File file = new File(config_path,"\\Config.csv");
		if(file.exists())
		{
			config = new Config(config_path,"\\Config.csv");
			if(config.file.length() > 0)
			{
				//si le fichier de configuration  existe et n'est pas vide - obtenons des flux références 
				links = config.getLinks();
				url_path   = links.get(links.size() - 1);
				isEmptyBase = false;
			}
				
			else
				isEmptyBase = true;
		}
		else
		{
			config= new Config(config_path,"\\Config.csv");
			isEmptyBase = true;
		}
			//Nous attribuons les propriétés de la classe FeedMessage
		  Title.setCellValueFactory(new PropertyValueFactory<>("title"));
	      Description.setCellValueFactory(new PropertyValueFactory<>("description"));
	      Date.setCellValueFactory(new PropertyValueFactory<>("date"));
	      Source.setCellValueFactory(new PropertyValueFactory<>("source"));
	      Link.setCellValueFactory(new PropertyValueFactory<>("link"));
	      Rss.setCellValueFactory(new PropertyValueFactory<>("rss"));
	      Author.setCellValueFactory(new PropertyValueFactory<>("author"));
	      Title.setMinWidth(120);
	      Description.setMinWidth(120);
	      Source.setMinWidth(120);
	      Link.setMinWidth(120);
	      Rss.setMinWidth(120);
	      Author.setMinWidth(120);
	      Date.setMinWidth(120);
	      //Création d'un champs d'interface
		Text text = new Text("Add rss to the base");

		TextField url = new TextField(url_path);
		Text text2 = new Text("Write the path folder");
		TextField path = new TextField(folder_path);
		Button rss_create = new Button("Create CSV");
		Button add_rss = new Button("Add");
		Button resetBase = new Button("Delete Database");
		resetBase.setOnAction(b -> ResetBase());
		add_rss.setOnAction(s -> addRss(url.getText(),path.getText()));
		rss_create.setOnAction(e -> isURL(url,path));
		VBox layout = new VBox(10);
		table.getColumns().addAll(Title, Description, Date,Source,Link,Rss,Author);
		layout.setPadding(new Insets(20,20, 20, 20));
		layout.getChildren().addAll(text,url,add_rss,resetBase,text2,path,rss_create,table);
		
		scene = new Scene(layout,880,700);
		window.setScene(scene);
		window.show();
		writeInCSV(path.getText());
	}
	// l'ajout d'un nouveau flux RSS
	private void addRss(String url,String path)
	{
		
		RSSFeedParser parser = new RSSFeedParser(url);
		String s  = parser.getFeedName();
		//ajouter dans le fichier de configuration et de métadonnées
		if(config.AddNewFlux(url, path, s))
		{
		// La création de fichiers de métadonnées
			Config meta = new Config(config_path,"\\Meta.csv");
			if(meta.AddNewFlux(url, path, s))
				meta.showAlertInfo();
		//Obtenir des liens à partir d'un fichier de métadonnées	
			metaLinks = meta.getLinks();
			links = config.getLinks();
			//l'enlèvement de métafichier
			meta.file.delete();
		}
		
		
		
	}
	//L'enregistrement d'un fichier csv
	private void writeInCSV(String path) 
	{
		MyFile w = new CSVFile(path,"RSS");
		
		Feed feed = null;
		List<FeedMessage> parsedFeed = new ArrayList<FeedMessage>();
		File file = new File(config_path,"\\Meta.csv");
		//Si le fichier de métadonnées existe - lire ce
		if( file.exists() &&!isEmptyBase)
		{
			for(String link: metaLinks)
			{
				RSSFeedParser parser = new RSSFeedParser(link);
				feed = parser.readFeed();
				String[] entries = null;
				//recevoir des flux à partir de la base de données
				for(FeedMessage message: feed.getMessages())
				{
					
					String descr = message.getDescription();
					String article = message.getTitle();
					// divisé en mots
					String[] split_article = article.split("\\W\\&");
					String[] split_descr = descr.split("\\W\\&");
					String final_descr = addStr(split_descr);
					String final_article = addStr(split_article);
					System.out.println(final_descr);
					String s = String.format("%s%s%s%s%s%s", final_article,final_descr,message.getDate(),message.getRss(),message.getAuthor(),message.getLink());
					//supprimer le caractère délimiteur
					entries = s.split("\t");
					FeedMessage msg = new FeedMessage();
					msg.setParsed(final_article, final_descr, message.getDate(), message.getAuthor(), message.getLink(), message.getSource(), message.getRss());
					parsedFeed.add(msg);
					w.write(entries);
				}
				// enregistrement de la table d'interface
				list.addAll(parsedFeed);
				
			    table.setItems(list);
			    
			}
			
			
		}
		
		
		else
		{
			
		if(config.file.length() >0)
		{
			//réécrire le fichier de destination
			try {
		        FileWriter fstream1 = new FileWriter(path +"\\RSS.csv");
		        BufferedWriter out1 = new BufferedWriter(fstream1); 
		        out1.write(""); 
		         out1.close(); 
		         } catch (Exception e) 
		            {System.err.println("Error in file cleaning: " + e.getMessage());}
			//lire les flux à partir du fichier de configuration
		for(String link: links)
		{
			System.out.println(link);
			RSSFeedParser parser = new RSSFeedParser(link);
			feed = parser.readFeed();
			String[] entries = null;
			for(FeedMessage message: feed.getMessages())
			{
				
				String descr = message.getDescription();
				String article = message.getTitle();
				String[] split_article = article.split("\\W\\&");
				String[] split_descr = descr.split("\\W\\&");
				String final_descr = addStr(split_descr);
				String final_article = addStr(split_article);
				String s = String.format("%s%s%s%s%s%s", final_article,final_descr,message.getDate(),message.getRss(),message.getAuthor(),message.getLink());
				entries = s.split("\t");
				FeedMessage msg = new FeedMessage();
				msg.setParsed(final_article, final_descr, message.getDate(), message.getAuthor(), message.getLink(), message.getSource(), message.getRss());
				parsedFeed.add(msg);
				w.write(entries);
			}
			
			list = getUserList(parsedFeed);
			
		    table.setItems(list);
		    
		}
		}else
			showAlertWarning();
		
		}
		
		
	}
	//vérifier le lien
	private boolean isURL(TextField text_url,TextField text_path) 
	{
		
		try{
			
			String str = text_url.getText();
			String path = text_path.getText();
			URL url = new URL(str);
			writeInCSV(path);
			return true;
		}
		catch(MalformedURLException ex)
		{
			System.out.println("Wrong form of url " + ex);
			return false;
		}
	}
	//transformer tableau d'entrée en une chaîne
	private String addStr(String[] entries)
	{
		String str = " ";
		for (String s: entries)
			str += s + " ";
		return str;
	}
	//les méthodes de notification d'utilisateur
	private void showAlertWarning() { 
	    Alert alert = new Alert(Alert.AlertType.WARNING);
	    alert.setTitle("Warning");
	    alert.setHeaderText(null);
	    alert.setContentText("Base is empty. Please add rss-chanel");
	    alert.showAndWait();
	}
	private void showInfoBaseReset() { 
	    Alert alert = new Alert(Alert.AlertType.INFORMATION);
	    alert.setTitle("Reset");
	    alert.setHeaderText(null);
	    alert.setContentText("Now base is empty");
	    alert.showAndWait();
	}
	//suppression de base de données
	private void ResetBase()
	{
		try {
	        FileWriter fstream1 = new FileWriter(config_path +"\\Config.csv");
	        BufferedWriter out1 = new BufferedWriter(fstream1); 
	        out1.write(""); 
	         out1.close(); 
	         showInfoBaseReset();
	         } catch (Exception e) 
	            {System.err.println("Error in file cleaning: " + e.getMessage());}

	}
	//l'ajout de la collection pour afficher
	  private ObservableList<FeedMessage> getUserList(List<FeedMessage> message) {
	 
	 
	      ObservableList<FeedMessage> list = FXCollections.observableArrayList(message);
	      return list;
	  }
	

}
